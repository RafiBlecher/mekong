Mekong
======

About
-----
UNSW's COMP2041 (Tools and Techniques for Software Construction) has 2
assignments. The second is usually some form of CGI website.

This year (2013), assigment 2 was to create Mekong, an online bookstore
that supported a number of features.

This is my assignment (received full marks), uploaded to bitbucket

1. To show to potential employees
2. Because the CSE has a file limit and this was rather large

Features
--------

1. Email verification
2. Password reset via email
3. Temporary baskets, stored in cookies
4. (Mostly) Secure pages. I tried to implement as much security as I understood, including secure cookies, and password management as covered [here](https://crackstation.net/hashing-security.htm)
    - There are issues though:
        1. HTML injection in forms
        2. JQuery Validator is the measly line of defence between valid data and explosions of errors
        3. I don't even remember them now. But there are more.
5. Dispatch-Table-controlled page request management, easy to modify
6. Cool advanced search, relatively intelligent
7. Python program to convert git log to readable table

    Note that I had a commit hook that rejected commits if the commit message did not contain the substring "Time Taken: "

    diary.txt is the diary that we needed to submit with our assignments. You can generate it by running

        git log > gitlogdiary
        python diary.py > diary.txt

8. Neat UI, including a mobile interface. You have got to love Bootstrap's responsive design capabilities.

Dependencies
------------
Python uses the following libraries:

1. sqlite3
2. smtplib
3. email.mime.multipart
4. email.mime.text

Setup
-----
You'll need an SMTP server that you can use to send out emails. Change this on line 304 of mekong_backend.py:

    s = smtplib.SMTP('smtp.cse.unsw.edu.au')

You'll also have to load the book list into the database:

    python book_list_local.py

