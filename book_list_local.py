import re
import ast

import sqlite3 as lite

def load_book_list(cursor):
    f = open('books.json')
    formatted_file = f.read()
    f.close()

    # this eval is safe, see http://stackoverflow.com/a/988251
    book_list_dict = ast.literal_eval(formatted_file)

    for book_ISBN in book_list_dict:
        data = []
        for info in ['isbn', 'ean', 'smallimageurl', 'smallimagewidth',
                     'smallimageheight', 'mediumimageurl', 'mediumimagewidth',
                     'mediumimageheight', 'largeimageurl', 'largeimagewidth',
                     'largeimageheight', 'catalog', 'binding', 'numpages',
                     'productdescription', 'publisher', 'publication_date',
                     'releasedate', 'authors', 'salesrank', 'price', 'title', 'year']:
            if info in book_list_dict[book_ISBN].keys() :
                data.append(str(book_list_dict[book_ISBN][info]))
            else:
                data.append('-')


        if not book_exists(cursor, book_ISBN):
            add_book(cursor, data)

def add_book(cursor, book):
    insert_command = 'INSERT INTO Books VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

    cursor.execute(insert_command, book)

def book_exists(cursor, ISBN):
    search_command = 'SELECT * FROM Books WHERE ISBN = "%s"'

    return cursor.execute(search_command % ISBN).fetchall() != []



conn = lite.connect('allInformation.db')
cursor = conn.cursor()
cursor.execute('create table if not exists Books (ISBN integer, EAN integer, smallimageurl text, smallimagewidth integer, smallimageheight integer, mediumimageurl text, mediumimagewidth integer, mediumimageheight integer, largeimageurl text, largeimagewidth integer, largeimageheight integer, catalog text, binding text, numpages integer, productdescription text, publisher text, publicationdate text, releasedate text, authors text, salesrank integer, price real, title text, year integer)')
cursor.execute('create table if not exists Baskets (user text, books text)')
cursor.execute('create table if not exists Users (username text, password text, salt text, fullname text, street text, cityOrSuburb text, state text, postcode text, emailaddress text)')

load_book_list(cursor)
conn.commit()
conn.close()
