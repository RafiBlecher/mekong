#!/usr/bin/python
import re

f = open('gitlogdiary').read().split('\n')

output = []

for line in f:
    if line.startswith('Date: '):
        date = ' '.join(line.split()[1:4])
        time = line.split()[4]
    if line.startswith('    '):
        info = re.match('    (.*) Time Taken: (.*)', line)
        comment = info.group(1)
        duration = info.group(2)

        output.append('| %-11s| %s | %-9s|%-11s| %-80s |' % (date, time, duration, '', comment))


print '''+------------+----------+----------+-----------+----------------------------------------------------------------------------------+
| Date       | End      | Duration | Activity  | Comments                                                                         |
+------------+----------+----------+-----------+----------------------------------------------------------------------------------+'''

print '\n+------------+----------+----------+-----------+----------------------------------------------------------------------------------+\n'.join(output[::-1])
