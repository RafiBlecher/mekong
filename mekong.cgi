#!/usr/bin/python2.7

import os

import sqlite3

import cgi
import cgitb
cgitb.enable()

import session

import mekong_frontend, mekong_backend
import results

from mekong_general import base_URL

def drop_me(cursor, cookie):
    cookie.expire_cookie()
    cursor.execute('DROP TABLE IF EXISTS Users')
    cursor.execute('DROP TABLE IF EXISTS Baskets')
    cursor.execute('DROP TABLE IF EXISTS Orders')

# This doesn't actually scrub input. It used to.
# But then it took an arrow to the knee.
# Now, it just converts CGI's FieldStorage to
# a more useable dict
def scrub_input(form):
    safe_form = {}
    for key in form.keys():
        if key == 'login' or key == 'update':
            # we don't care if they clicked login or update.
            # in fact, the only thing we ever care about is
            # if they click 'delete' on the Basket page
            continue
        value = form[key].value
        safe_form[key] = value
    return safe_form

class Dispatcher(object):
    def login(self):
        if not form.get('username') and not form.get('password'):
            mekong_frontend.serve_login_page(database, user_logged_in)
        elif form.get('username') and form.get('password'):
            password = form['password']
            username = form['username']
            if form.get('remember'):
                persistent_cookie = True
            else:
                persistent_cookie = False

            result = mekong_backend.user_okay(database, username, password)
            if result == results.SUCCESS_ACCOUNT_OKAY:
                # signed in with valid username/password! give'm a cookie!
                cookie.update(username, persistent_cookie = persistent_cookie)
                mekong_backend.merge_cookie_basket(database, username, cookie)

                mekong_frontend.redirect_to('search')
            else: # there was an error. display in on the login page.
                mekong_frontend.serve_login_page(database, user_logged_in, result)

    def search(self):
        if form:
            mekong_frontend.serve_search_results(database, user_logged_in, form)
        else:
            mekong_frontend.serve_search_page(database, user_logged_in)

    def new_account(self):
        if form.get('username') and form.get('emailaddress'):
            email_address = form['emailaddress']
            username = form['username']
            user_info = mekong_backend.get_user(database, username, email_address)
            if user_info == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
                mekong_backend.create_new_account(database, form)
                mekong_frontend.serve_new_account_successful(database, user_logged_in, form['emailaddress'])
            else:
                # if the email address requested was matched
                if user_info.get('emailaddress') == form['emailaddress']:
                    error = 'There is already an account with this email address. If this is your account, reset your password with the "Forgot Password" link on the Sign In page.'
                else: # it must have been the username that was matched
                    error = 'That username is taken. Please choose another username and try again.'
                mekong_frontend.serve_new_account_page(database, user_logged_in, error_message = error)
        else:
            mekong_frontend.serve_new_account_page(database, user_logged_in)

    def verify_account(self):
        if form.get('string'):
            string = form['string']
            user_validated = mekong_backend.verify_account(database, string)
            if user_validated == results.FAILURE_ACCOUNT_NO_VERIFICATION_STRING:
                error_message = 'Error verifying email address. Click <a href="{base}/resend_verification">here</a> to send a new verification email.'.format(base = base_URL)
                mekong_frontend.error(database, user_logged_in, error_message)
            elif user_validated == results.FAILURE_VERIFICATION_TIMEOUT:
                mekong_frontend.error(database, user_logged_in, results.FAILURE_VERIFICATION_TIMEOUT)
            else: # user_validated is the username
                cookie.update(user_validated)
                mekong_frontend.serve_account_verification_successful(database, user_logged_in)
        else:
            mekong_frontend.error(database, user_logged_in, 'Invalid link format.')

    # kills a validation link
    def kill(self):
        if form.get('string'):
            string = form['string']
            mekong_backend.kill_verification(database, string)
        mekong_frontend.redirect_to('search')

    def signout(self):
        cookie.expire_cookie()
        mekong_frontend.redirect_to('search')

    def send_email(self, page):
        email_address = form.get('emailaddress')
        if not email_address: # form has not been submitted, so serve form page
            if page == 'password':
                mekong_frontend.serve_email_password_reset_page(database, user_logged_in)
            else:
                mekong_frontend.serve_resend_verification_page(database, user_logged_in)
        else:
            reset = (page == 'password')
            result = mekong_backend.resend_verification_email(database, email_address, reset)

            if result == results.SUCCESS_VERIFICATION_EMAIL_SENT:
                page_to_serve = mekong_frontend.serve_verification_email_sent
                if reset:
                    page_to_serve = mekong_frontend.serve_new_password_email_sent
                page_to_serve(database, user_logged_in, email_address)

            elif result == results.FAILURE_VERIFICATION_IN_PROGRESS:
                message = 'Verification email sent in the last 15 minutes.'
                if reset:
                    message = 'Password reset email sent in the last 15 minutes.'

                mekong_frontend.error(database, user_logged_in, message)
            elif result == results.FAILURE_EMAIL_DOESNT_EXIST:
                mekong_frontend.error(database, user_logged_in, result)

    def resend_verification(self):
        self.send_email('verify')

    def forgotten_password(self):
        self.send_email('password')

    def reset_password(self):
        if form.get('string'):
            string = form['string']
            result = mekong_backend.get_info_from_string(database, string)
            if result == results.FAILURE_VERIFICATION_STRING_DOESNT_EXIST:
                mekong_frontend.error(database, user_logged_in, result)
            elif result == results.FAILURE_PASSWORD_RESET_TIMEOUT:
                mekong_frontend.error(database, user_logged_in, result)
            else:
                mekong_frontend.serve_new_password_page(database, user_logged_in, result)
        elif form.get('password') and form.get('username') and form.get('emailaddress'):
            email_address = form['emailaddress']
            password = form['password']
            username = form['username']
            mekong_backend.reset_password(database, username, password)
            mekong_frontend.serve_verification_email_sent(database, user_logged_in, email_address)
        else:
            # wat. get away from my reset link.
            mekong_frontend.redirect_to('search')

    def info(self):
        if form.get('ISBN'):
            ISBN = form['ISBN']
            mekong_frontend.serve_book_info(database, ISBN, user_logged_in)
        else:
            mekong_frontend.error(database, user_logged_in, 'Invalid link')

    def add_item(self):
        if form.get('ISBN'):
            ISBN = form['ISBN']
            result = mekong_backend.get_book_details(database, ISBN)
            if result == results.FAILURE_INVALID_ISBN:
                mekong_frontend.error(database, user_logged_in, result)
                return
            mekong_backend.update_basket(database, user_logged_in, cookie, ISBN, 'add')
            print "added" # this is a response message for jquery/javascript. it's read to check that the book is added to the basket successfully
        else:
            mekong_frontend.error(database, user_logged_in, 'Invalid link')

    def update_quantity(self):
        if form.get('ISBN') and form.get('quantity'):
            ISBN = form['ISBN']
            quantity = form['quantity']

            # so yes, this may cause a TypeError, but the function is called
            # by javascript, so if it errors, the behaviour is just that the
            # page doesn't update. it's sort of dirty, but there's a check
            # on the page that the quantity is a number, so this will only
            # matter if someone plays around with the HTML before submitting.
            quantity = int(quantity)

            if 'delete' in form.keys():
                quantity = 0

            result = mekong_backend.get_book_details(database, ISBN)
            # same as above. if you're playing around with the HTML stuff then
            # this will error, but if you're using the website properly then this
            # will be fine anyway. Leaving this if statement in so someone can
            # error or 404 or whatever if they want to.
            if result == results.FAILURE_INVALID_ISBN:
                pass

            mekong_backend.update_basket(database, user_logged_in, cookie, ISBN, 'update', quantity)
        else:
            mekong_frontend.error(database, user_logged_in, 'Malformed link.')

    def basket(self):
        mekong_frontend.serve_basket(database, user_logged_in, cookie)

    def checkout(self):
        result = mekong_frontend.serve_checkout_page(database, user_logged_in)
        if result == results.FAILURE_CHECKOUT_NO_BASKET:
            mekong_frontend.error(database, user_logged_in, result)

    def history(self):
        mekong_frontend.serve_order_history(database, user_logged_in)

    def save_order(self):
        if form.get('name') and form.get('cardnumber') and form.get('month') and form.get('year') and form.get('ccv'):
            result = mekong_backend.save_basket_to_history(database, user_logged_in)
            if result == results.FAILURE_ACCOUNT_BASKET_DOESNT_EXIST:
                mekong_frontend.error(database, user_logged_in, result)
            else:
                mekong_frontend.serve_order_saved_successful(database, user_logged_in)
        else:
            mekong_frontend.error(database, user_logged_in, 'The form data on the checkout page was incomplete. Please try to check out again.')

    def error(self):
        mekong_frontend.error(database, user_logged_in, '<span class="error">You must be logged in to do that.</span>')

    def error_404(self):
        mekong_frontend.error_404(database, user_logged_in)

cookie = session.Session()

database = sqlite3.connect('allInformation.db')
mekong_backend.establish_database(database)

page_requested = os.environ.get('PATH_INFO', '/')[1:] # get rid of /

form = scrub_input(cgi.FieldStorage())

user_logged_in = cookie.is_logged_in()

# how did you manage that? =/ You're logged in, but your username isn't in the database!
if user_logged_in and not mekong_backend.fullname_from_username(database, user_logged_in):
    cookie.expire_cookie()

# force change to .../mekong.cgi/search because elegant solutions require brainpower
if not page_requested or (page_requested == 'login' and user_logged_in):
    print 'Status: 200 OK'
    print 'Content-Type: text/html'
    print

    mekong_frontend.redirect_to('search')

# You have to be logged in to access these pages.
restricted_pages = ( 'signout', 'checkout', 'history', 'save_order' )

#######################
# Dispatcher is a sexy data structure. It's like a dispatch table. I give you a string
# (a function name) and you give me back a function that I can run. This is my solution
# to dealing with page handling - before the dispatcher, I had about 200 lines of
# if-elif-else
#######################
dispatcher = Dispatcher()

try:
    action = getattr(dispatcher, page_requested)

    print cookie.cookie # send cookie to browser
    print 'Status: 200 OK'
except AttributeError:
    # there's no handler in the dispatch table for the requested page
    action = getattr(dispatcher, 'error_404')

    print 'Status: 404 Not Found'

print 'Content-Type: text/html'
print

if not user_logged_in and page_requested in restricted_pages:
    action = getattr(dispatcher, 'error')

action()
