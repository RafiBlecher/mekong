import os, hashlib

import re, ast

import time

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import results
from mekong_general import base_URL as script_URL


# Weights for search-matching
MATCH_KEYWORD = 1
MATCH_PUBLISHER = 160
MATCH_AUTHOR = 300
MATCH_TITLE = 600
MATCH_ISBN = 1200

EMAIL_TIMEOUT_MINUTES = 15

class database_command:
    def __init__(self):
        self.operation = '' # 'INSERT', 'SELECT', 'UPDATE'

        self.query_table = '' # the table we want to check

        self.requested_columns = ()
        self.values_to_insert = ()
        self.values_to_set = {}

        self.query_terms = {} # column, value pairs
        self.query_LIKE = False

        # Seeing as no one's going to ask for 0 results, I'm using 0 to represent 'all results'
        # for requests
        self.ALL_RESULTS = 0

        # Logical connector for requests. AND is default, but can be set to OR
        self.link = 'AND'

        # default number of results, but can be adjusted
        self.num_results = 1

    def execute(self, database):
        cursor = database.cursor()
        if self.operation == 'INSERT':
            # Need to maintain SQLite binding to avoid SQL injections
            operation = 'INSERT INTO {table} VALUES ({binding_tuple})'

            binding_tuple = ', '.join('?' for value in self.values_to_insert)

            execute_instruction = operation.format(table = self.query_table, binding_tuple = binding_tuple)

            cursor.execute(execute_instruction, self.values_to_insert)
            database.commit()

        if self.operation == 'SELECT':
            operation = 'SELECT {columns} FROM {table} WHERE {parameters}'

            key_val_pairs = tuple(self.query_terms.iteritems())
            if self.query_LIKE:
                args = ('{k} LIKE ?'.format(k = k, v = v) for k, v in key_val_pairs)
            else:
                args = ('{k} = ?'.format(k = k, v = v) for k, v in key_val_pairs)

            link = ' '+self.link+' '
            parameters = link.join(args)

            columns = ', '.join(self.requested_columns)

            execute_instruction = operation.format(columns = columns, table = self.query_table, parameters = parameters)
            query_results = cursor.execute(execute_instruction, tuple(pair[1] for pair in key_val_pairs))

            # I'm aware that I could simplify this by doing .fetchmany(self.num_results) even when self.num_results is 1,
            # but that feels a bit dirty
            if self.num_results == 1:
                return query_results.fetchone()
            elif self.num_results == self.ALL_RESULTS:
                return query_results.fetchall()
            else:
                return query_results.fetchmany(self.num_results)

        if self.operation == 'UPDATE':
            operation = 'UPDATE {table} SET {values} WHERE {parameters}'

            values_pairs = tuple(self.values_to_set.iteritems())
            args = ('{k} = ?'.format(k = k, v = v) for k, v in values_pairs)
            values = ', '.join(args)

            params_pairs = tuple(self.query_terms.iteritems())
            args = ('{k} = ?'.format(k = k, v = v) for k, v in params_pairs)
            parameters = ' AND '.join(args)

            execute_instruction = operation.format(table = self.query_table, values = values, parameters = parameters)

            cursor.execute(execute_instruction, tuple(pair[1] for pair in values_pairs) + tuple(pair[1] for pair in params_pairs))
            database.commit()

def establish_database(database):
    cursor = database.cursor()
    cursor.execute('create table if not exists Books (ISBN integer, EAN integer, smallimageurl text, smallimagewidth integer, smallimageheight integer, mediumimageurl text, mediumimagewidth integer, mediumimageheight integer, largeimageurl text, largeimagewidth integer, largeimageheight integer, catalog text, binding text, numpages integer, productdescription text, publisher text, publicationdate text, releasedate text, authors text, salesrank integer, price real, title text, year integer)')
    cursor.execute('create table if not exists Baskets (user text, books blob)')
    cursor.execute('create table if not exists Orders (user text, orders blob)')
    cursor.execute('create table if not exists Users (username text, password text, salt text, fullname text, street text, cityOrSuburb text, state text, postcode text, emailaddress text, timeoutstart blob, verificationstring text, accountverified integer)')

def random_string():
    return os.urandom(50).encode('hex')

def hash_password(password, salt = None):
    if not salt:
        salt = random_string()
    hashed_password = hashlib.sha256(salt + password).hexdigest().encode('hex')

    return (hashed_password, salt)

def create_new_account(database, form):
    # sqlite3 takes values in the order of the columns defined
    # so we must extract information in the same order. Note
    # that the password is being hashed (with salt), so we
    # must be careful to enter the hashed password and salt in
    # their respective locations

    values = []
    for k in ['username', 'password', 'fullname', 'street', 'cityOrSuburb', 'state', 'postcode', 'emailaddress']:
        value = form[k]
        if k == 'password':
            hashed_password, salt = hash_password(value)
            values.append(hashed_password)
            values.append(salt)
        else:
            values.append(value)

    verification_string = random_string()
    values.append(time.time()) # needed for activation link to timeout
    values.append(verification_string)
    values.append(0) # not verified

    insert_user = database_command()
    insert_user.operation = 'INSERT'
    insert_user.query_table = 'Users'
    insert_user.values_to_insert = values
    insert_user.execute(database)

    username = form['username']

    insert_basket = database_command()
    insert_basket.operation = 'INSERT'
    insert_basket.query_table = 'Baskets'
    insert_basket.values_to_insert = (username, '{}')
    insert_basket.execute(database)

    insert_orders = database_command()
    insert_orders.operation = 'INSERT'
    insert_orders.query_table = 'Orders'
    insert_orders.values_to_insert = (username, '[]')
    insert_orders.execute(database)

    email_verification(form['emailaddress'], verification_string)

def correct_password(database, username, password):
    select_password_info = database_command()
    select_password_info.operation = 'SELECT'
    select_password_info.query_table = 'Users'
    select_password_info.requested_columns = ('password', 'salt')
    select_password_info.query_terms = {'username': username}

    saved_password_hash, password_salt = select_password_info.execute(database)

    if hash_password(password, password_salt)[0] == saved_password_hash:
        return results.SUCCESS_ACCOUNT_CORRECT_PASSWORD

    return results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD

def get_user(database, username, address = None):
    select_user_info = database_command()
    select_user_info.operation = 'SELECT'
    select_user_info.query_table = 'Users'

    if address:
        select_user_info.requested_columns = ('username', 'emailaddress')
        select_user_info.link = 'OR'
        select_user_info.query_terms = {'username': username, 'emailaddress': address}

        result = select_user_info.execute(database)
        if result:
            return {'username': result[0], 'emailaddress': result[1]}
    else:
        select_user_info.requested_columns = ('username',)
        select_user_info.query_terms = {'username': username}

        result = select_user_info.execute(database)
        if result:
            return {'username': result[0], 'emailaddress': ''}
    return results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD

def account_verified(database, user):
    select_account_verified = database_command()
    select_account_verified.operation = 'SELECT'
    select_account_verified.query_table = 'Users'
    select_account_verified.requested_columns = ('accountverified',)
    select_account_verified.query_terms = {'username': user}

    result = select_account_verified.execute(database)
    if not result:
        return results.FAILURE_ACCOUNT_NO_VERIFICATION_STRING

    verified = int(result[0])
    if not verified:
        return results.FAILURE_ACCOUNT_NOT_VERIFIED

    return results.SUCCESS_ACCOUNT_VERIFIED

def user_okay(database, username, password):
    account_is_verified = account_verified(database, username)
    user_in_database = get_user(database, username)

    if account_is_verified == results.FAILURE_ACCOUNT_NO_VERIFICATION_STRING and\
       user_in_database != results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
        return account_is_verified

    if account_is_verified == results.FAILURE_ACCOUNT_NOT_VERIFIED:
        return account_is_verified

    if user_in_database == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
        return user_in_database

    password_is_correct = correct_password(database, username, password)
    if password_is_correct == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
        return password_is_correct

    return results.SUCCESS_ACCOUNT_OKAY

def reset_password(database, username, password):
    verification_string = random_string()
    timeoutstart = time.time() # needed for activation link to timeout
    verified = 0

    hashed_password, salt = hash_password(password)

    update_password_info = database_command()
    update_password_info.operation = 'UPDATE'
    update_password_info.query_table = 'Users'
    update_password_info.values_to_set = {'timeoutstart': timeoutstart,
                                          'verificationstring': verification_string,
                                          'accountverified': verified,
                                          'password': hashed_password,
                                          'salt': salt}
    update_password_info.query_terms = {'username': username}

    update_password_info.execute(database)

    address = get_emailaddress_from_username(database, username)
    email_verification(address, verification_string)

def email_verification(address, verification_string, reset = False):
    # A lot of this code was taken from http://docs.python.org/2/library/email-examples.html#id5
    # No, I didn't know how to do it, but I understand now! And I figure that this is better than
    # doing a sys call
    msg = MIMEMultipart('alternative')
    msg['From'] = 'rafi.noreply@mekong.com'
    msg['To'] = address

    if reset:
        msg['Subject'] = 'Password Reset - Do Not Reply'
        text = "This message was sent to reset your password at mekong.com.\n\nTo reset your password, please go to {base}reset_password?string={verification_string}.\n\nIf this email is not meant for you, please go to {base}kill?string={verification_string}.\n\n- Mekong"

        html = """
<html>
    <head></head>
        <body>
            This message was sent to reset your password mekong.com.<br /><br />
            To reset your password please follow <a href="{base}reset_password?string={verification_string}">this link</a>.<br /><br />
            If this email is not meant for you, please follow <a href="{base}kill?string={verification_string}">this link</a>.<br /><br />
            - Mekong
      </body>
    </html>
    """

    else:
        msg['Subject'] = 'Account Verification - Do Not Reply'
        text = "This message was sent to verify an account at mekong.com.\n\nPlease verify your account by going to {base}verify_account?string={verification_string}.\n\nIf this email is not meant for you, please go to {base}kill?string={verification_string}.\n- Mekong"


        html = """
<html>
    <head></head>
        <body>
            This message was sent to verify an account at mekong.com.<br /><br />
            Please verify your account by following <a href="{base}verify_account?string={verification_string}">this link</a>.<br /><br />
            If this email is not meant for you, please follow <a href="{base}kill?string={verification_string}">this link</a>.<br /><br />
            - Mekong
      </body>
    </html>
    """

    base = "http://cgi.cse.unsw.edu.au{script_URL}/".format(script_URL = script_URL)
    msg.attach(MIMEText(text.format(base = base, verification_string = verification_string), 'plain'))
    msg.attach(MIMEText(html.format(base = base, verification_string = verification_string), 'html'))

    s = smtplib.SMTP('smtp.cse.unsw.edu.au')

    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()

def resend_verification_email(database, address, reset = False):
    select_verification_info = database_command()
    select_verification_info.operation = 'SELECT'
    select_verification_info.query_table = 'Users'
    select_verification_info.requested_columns = ('timeoutstart', 'accountverified', 'verificationstring')
    select_verification_info.query_terms = {'emailaddress': address}

    result = select_verification_info.execute(database)

    if not result:
        return results.FAILURE_EMAIL_DOESNT_EXIST
    timeoutstart, account_verified, verification_string = result

    seconds_since_account_created = time.time() - float(timeoutstart)
    if (seconds_since_account_created / 60) < EMAIL_TIMEOUT_MINUTES:
        return results.FAILURE_VERIFICATION_IN_PROGRESS

    verification_string = random_string()

    update_verification_info = database_command()
    update_verification_info.operation = 'UPDATE'
    update_verification_info.query_table = 'Users'
    update_verification_info.query_terms = {'emailaddress': address}
    update_verification_info.values_to_set = {'timeoutstart': time.time(),
                                              'verificationstring': verification_string,
                                              'accountverified': account_verified
                                             }

    if reset:
        update_verification_info.values_to_set['password'] = ''
        update_verification_info.values_to_set['salt'] = ''

    update_verification_info.execute(database)

    email_verification(address, verification_string, reset)

    return results.SUCCESS_VERIFICATION_EMAIL_SENT

def verify_account(database, verification_string):
    select_verification_info = database_command()
    select_verification_info.operation = 'SELECT'
    select_verification_info.query_table = 'Users'
    select_verification_info.requested_columns = ('timeoutstart', 'username')
    select_verification_info.query_terms = {'verificationstring': verification_string}

    result = select_verification_info.execute(database)

    if not result:
        return results.FAILURE_ACCOUNT_NO_VERIFICATION_STRING

    timeoutstart, user = result

    seconds_since_account_created = time.time() - float(timeoutstart)

    # invalidate link after use
    kill_verification(database, verification_string, verified = 1)

    # verified in under 15m?
    if (seconds_since_account_created / 60) < EMAIL_TIMEOUT_MINUTES:
        return user
    return results.FAILURE_VERIFICATION_TIMEOUT

def kill_verification(database, verification_string, verified = 0):
    update_verification_info = database_command()
    update_verification_info.operation = 'UPDATE'
    update_verification_info.query_table = 'Users'
    update_verification_info.values_to_set = {'timeoutstart': 0,
                                              'verificationstring': "",
                                              'accountverified': verified
                                             }
    update_verification_info.query_terms = {'verificationstring': verification_string}

    update_verification_info.execute(database)


def get_book_details(database, ISBN):
    select_book_details = database_command()
    select_book_details.operation = 'SELECT'
    select_book_details.query_table = 'Books'
    select_book_details.requested_columns = ('*')
    select_book_details.query_terms = {'ISBN': ISBN}

    result = select_book_details.execute(database)
    if not result:
        return results.FAILURE_INVALID_ISBN

    return result

def add(values, value):
    if value not in values or value == MATCH_KEYWORD:
        values.append(value)
    return values

def strip_characters(word):
    characters = '()\'-,'
    return re.sub(characters, '', word)

def find_all_matching_books(database, search_terms, search_limit):
    matches = {}
    multipliers = {}

    for key, value in search_terms.iteritems():
        if key == 'ISBN':
            # match by ISBN is most strict, so have that as the first search result
            # we don't use the value now, but we stick it at the front of the list
            match_for_ISBN = get_book_details(database, value)
            if match_for_ISBN != results.FAILURE_INVALID_ISBN:
                match_for_ISBN = match_for_ISBN[0] # get the ISBN part
                matches[match_for_ISBN] = add(matches.get(match_for_ISBN, []), MATCH_ISBN)

        if key == 'publisher':
            matches_for_publisher = get_books_by_publisher(database, value)
            for match in matches_for_publisher:
                match = match[0] # get the ISBN part
                matches[match] = add(matches.get(match, []), MATCH_PUBLISHER)

        if key == 'title':
            matches_for_title = get_books_by_title(database, value)
            for match in matches_for_title:
                matches[match] = add(matches.get(match, []), MATCH_TITLE)

        if key == 'author':
            matches_for_author = get_books_by_author(database, value)
            for match in matches_for_author:
                match = match[0] # get the ISBN part
                matches[match] = add(matches.get(match, []), MATCH_AUTHOR)

        if key == 'contains':
            for keyword in value.split(' '):
                if keyword in ('and', 'the', 'or', 'of'):
                    continue # don't match these words. there are probably more to exclude, but these are the obvious ones
                keyword = strip_characters(keyword)
                matches_for_contains = get_book_contains(database, keyword)
                for match, num_matches in matches_for_contains.iteritems():
                    # this has to be kept as a set so that the same keyword is not multiple times, even if it appears more than
                    # once in the search string
                    multipliers[match] = multipliers.get(match, set([]))
                    multipliers[match].add(keyword)
                    for i in range(num_matches):
                        matches[match] = add(matches.get(match, []), MATCH_KEYWORD)

    matches = sorted(matches, key=lambda k: score( matches[k], multipliers.get(k, []) ), reverse = True)
    return len(matches), matches[:search_limit]

def score(values, unique_keywords_matched):
    multiplier = 4**(len(unique_keywords_matched))
    score =  sum(v for v in values if v == MATCH_KEYWORD) * multiplier
    score += sum(v for v in values if v != MATCH_KEYWORD)
    return score

def clean_percent(string):
    return '%'+re.sub('%', '%%', string)+'%'

def get_book_contains(database, contains):
    matches = {}

    select_books_contains = database_command()
    select_books_contains.operation = 'SELECT'
    select_books_contains.query_table = 'Books'
    select_books_contains.requested_columns = ('*')
    select_books_contains.query_terms = {'authors||title||productdescription': clean_percent(contains)}
    select_books_contains.query_LIKE = True
    select_books_contains.num_results = select_books_contains.ALL_RESULTS

    for book in select_books_contains.execute(database):
        collated_info = " ".join(str(col) for col in book).lower()
        matches[book[0]] = collated_info.count(contains.lower())

    return matches

def get_books_by_publisher(database, publisher):
    select_publisher_matches = database_command()
    select_publisher_matches.operation = 'SELECT'
    select_publisher_matches.query_table = 'Books'
    select_publisher_matches.requested_columns = ('ISBN', )
    select_publisher_matches.query_terms = {'publisher': clean_percent(publisher)}
    select_publisher_matches.query_LIKE = True
    select_publisher_matches.num_results = select_publisher_matches.ALL_RESULTS

    return select_publisher_matches.execute(database)

def get_books_by_author(database, author):
    select_author_matches = database_command()
    select_author_matches.operation = 'SELECT'
    select_author_matches.query_table = 'Books'
    select_author_matches.requested_columns = ('ISBN', )
    select_author_matches.query_terms = {'authors': clean_percent(author)}
    select_author_matches.query_LIKE = True
    select_author_matches.num_results = select_author_matches.ALL_RESULTS

    return select_author_matches.execute(database)


def get_books_by_title(database, title):
    select_title_matches = database_command()
    select_title_matches.operation = 'SELECT'
    select_title_matches.query_table = 'Books'
    select_title_matches.requested_columns = ('ISBN', 'title')
    select_title_matches.query_terms = {'title': clean_percent(title)}
    select_title_matches.query_LIKE = True
    select_title_matches.num_results = select_title_matches.ALL_RESULTS

    matches = []

    for book in select_title_matches.execute(database):
        title = re.escape(title)
        if re.search("(^| )"+title+"( |$)", book[1], flags=re.I):
            matches.append(book[0])

    return matches

def get_emailaddress_from_username(address, username):
    return get_user_details(address, username)[8]

def fullname_from_username(address, username):
    user_info = get_user_details(address, username)
    if user_info:
        return user_info[3]
    else:
        return results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD

def merge_cookie_basket(database, username, cookie):
    cookie_basket = cookie.get_basket()
    if not cookie_basket or not username:
        # there's nothing in your cookie basket, or you're not logged in. what am I merging?
        return

    basket = get_users_basket(database, username)

    for ISBN in cookie_basket:
        basket[ISBN] = basket.get(ISBN, 0) + cookie_basket[ISBN]
    cookie.data['basket'] = {}

    update_basket = database_command()
    update_basket.operation = 'UPDATE'
    update_basket.query_table = 'Baskets'
    update_basket.values_to_set = {'books': str(basket)}
    update_basket.query_terms = {'user': username}

    update_basket.execute(database)

def update_basket(database, user, cookie, ISBN, action, count = 1):
    if not user:
        cookie_basket = cookie.get_basket()
        if action == 'add' and cookie_basket.get(ISBN):
            cookie_basket[ISBN] += count
        else:
            cookie_basket[ISBN] = count

        if count == 0:
            del cookie_basket[ISBN]
        cookie.data['basket'] = cookie_basket
        return

    basket = get_users_basket(database, user)

    if action == 'add' and basket.get(ISBN):
        basket[ISBN] += count
    else:
        basket[ISBN] = count

    if count == 0:
        del basket[ISBN]

    update_basket = database_command()
    update_basket.operation = 'UPDATE'
    update_basket.query_table = 'Baskets'
    update_basket.values_to_set = {'books': str(basket)}
    update_basket.query_terms = {'user': user}

    update_basket.execute(database)


def get_users_basket(database, username):
    if not username:
        return results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD

    select_users_basket = database_command()
    select_users_basket.operation = 'SELECT'
    select_users_basket.query_table = 'Baskets'
    select_users_basket.requested_columns = ('books', )
    select_users_basket.query_terms = {'user': username}

    result = select_users_basket.execute(database)
    if not result:
        # there's no basket, but because temporary (logged-out) baskets are allowed,
        # this doesn't matter.
        return
    basket = ast.literal_eval(result[0])

    return basket

def get_user_details(database, user):
    select_user_details = database_command()
    select_user_details.operation = 'SELECT'
    select_user_details.query_table = 'Users'
    select_user_details.requested_columns = ('*')
    select_user_details.query_terms = {'username': user}

    user_info = ast.literal_eval(str(select_user_details.execute(database)))

    return user_info

def save_basket_to_history(database, user):
    basket = get_users_basket(database, user)
    if not basket:
        return results.FAILURE_ACCOUNT_BASKET_DOESNT_EXIST
    basket['timesubmitted'] = time.time()

    orders = get_order_history(database, user)
    orders.append(basket)

    update_orders = database_command()
    update_orders.operation = 'UPDATE'
    update_orders.query_table = 'Orders'
    update_orders.values_to_set = {'orders': str(orders)}
    update_orders.query_terms = {'user': user}

    update_orders.execute(database)

    # clear basket after saved to history, because... why would you want it to stay there?
    update_orders = database_command()
    update_orders.operation = 'UPDATE'
    update_orders.query_table = 'Baskets'
    update_orders.values_to_set = {'books': "{}"}
    update_orders.query_terms = {'user': user}

    update_orders.execute(database)

def get_order_history(database, user):
    select_order_history = database_command()
    select_order_history.operation = 'SELECT'
    select_order_history.query_table = 'Orders'
    select_order_history.requested_columns = ('orders',)
    select_order_history.query_terms = {'user': user}

    orders = ast.literal_eval(select_order_history.execute(database)[0])

    return orders

def get_info_from_string(database, string):
    select_user_info = database_command()
    select_user_info.operation = 'SELECT'
    select_user_info.query_table = 'Users'
    select_user_info.requested_columns = ('username', 'emailaddress', 'timeoutstart')
    select_user_info.query_terms = {'verificationstring': string}

    result = select_user_info.execute(database)

    if result:
        timeoutstart = result[2]
        seconds_since_email_sent = time.time() - float(timeoutstart)

        kill_verification(database, string, verified = 1)

        if (seconds_since_email_sent / 60) > EMAIL_TIMEOUT_MINUTES:
            return results.FAILURE_PASSWORD_RESET_TIMEOUT

        return result[:-1]
    return results.FAILURE_VERIFICATION_STRING_DOESNT_EXIST
