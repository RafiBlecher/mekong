import mekong_backend
import ast
import re

import time

import results
from mekong_general import base_URL

import templater

SEARCH_RESULT_LIMIT = 40

def serve_login_page(database, logged_in, error = None):
    title = 'Login'
    if error:
        title = 'Error - Login'

    error_message = error or ''

    template_vars = {'title': title,
                     'message': error_message,
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'sign in')
                    }

    templater.render('login.html', template_vars)

def serve_new_account_page(database, logged_in, error_message = ''):
    title = 'New Account'
    if error_message:
        title = 'Error - New Account'

    template_vars = {'title': title,
                     'message': error_message,
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'register')
                    }

    templater.render('new_account.html', template_vars)

def success_page(database, logged_in, page, success_message, information):
    template_vars = {'title': 'Success!',
                     'success_message': success_message,
                     'information': information,
                     'nav': print_nav(database, logged_in, 'new account')
                    }

    templater.render('success.html', template_vars)

def serve_account_verification_successful(database, logged_in):
    information = "Click 'Search' above to start searching for books!"

    success_page(database, logged_in, 'verify_account', 'Account successfully verified.', information)

def serve_order_saved_successful(database, user_logged_in):
    information = "Your order has been processed. It will be delivered within 10 work days. Your order is now available in your Order History (link in the menu bar)."

    success_page(database, user_logged_in, 'order_successful', 'Order processed.', information)

def one_more_step(database, logged_in, page, message, nav_page, title = 'One more step...'):
    template_vars = {'title': title,
                     'message': message,
                     'base_URL': base_URL,
                     'page': page,
                     'nav': print_nav(database, logged_in, nav_page)
                    }

    templater.render('one_more_step.html', template_vars)

def serve_new_account_successful(database, logged_in, address):
    one_more_step(database, logged_in, 'resend_verification', 'New account successfully made.', 'new account')

def serve_verification_email_sent(database, logged_in, address):
    one_more_step(database, logged_in, 'resend_verification', 'Your password has been reset and an email has been sent to your address to verify your account.', 'verification sent')

def serve_new_password_email_sent(database, logged_in, address):
    one_more_step(database, logged_in, 'forgotten_password', 'Password reset link sent.', 'new password', title = 'Email sent...')

def redirect_to(page):
    print '<script type="text/javascript">document.location.href="{base_URL}/{page}"</script> '.format(base_URL = base_URL, page = page)

def search_page_template(basket = False):
    advanced_search = """
                        <h3>Advanced Search</h3>
                        <form method = 'get' action='{base_URL}/search'>
                            <div class='form-group'>
                                <label for='title'>Title</label>
                                <input type='text' class='form-control' name='title' id='title'>
                            </div>
                            <div class='form-group'>
                                <label for='contains'>Keywords</label>
                                <input type='text' class='form-control' name='contains' id='contains'>
                            </div>
                            <div class='form-group'>
                                <label for='Author'>Author</label>
                                <input type='text' class='form-control' name='author' id='author'>
                            </div>
                            <div class='form-group'>
                                <label for='publisher'>Publisher</label>
                                <input type='text' class='form-control' name='publisher' id='publisher'>
                            </div>
                            <div class='form-group'>
                                <label for='ISBN'>ISBN</label>
                                <input type='text' class='form-control' name='ISBN' id='ISBN'>
                            </div>
                            <div class='form-group'>
                                <label for='result-cap'>Number of Results</label>
                                <input type='text' class='form-control' name='result-cap' id='result-cap'>
                            </div>

                            <button type='submit' class='btn btn-default pull-right'>Advanced Search</button>
                        </form>""".format(base_URL = base_URL)

    if basket:
        search_message = "Search for books to add to your basket"
    else:
        search_message = "Search for keywords"

    keyword_search = """
                        <form class='search' method='get' action='{base_URL}/search'>
                            <div class='form-group'>
                                <label for='keywordsearch'>{search_message}</label>
                                <div class="input-group">
                                    <input type='text' placeholder="e.g. Harry Potter Sorcerer's Stone" class='form-control' name='contains' id='keywordsearch'>
                                    <span class="input-group-btn">
                                        <button type='submit' class='btn btn-default'>Search</button>
                                    </span>
                                </div>
                            </div>

                        </form>""".format(base_URL = base_URL, search_message = search_message)
    if basket:
        return advanced_search, keyword_search


    html = ''
    html += """
        <div class='container search-page'>
            <div class='row'>
                <div class='col-xs-3 advanced-search-wrapper'>
                    <div class='advanced-search'>
                    {advanced_search}
                    </div> <!-- advanced-search -->
                </div> <!-- advanced-search-wrapper -->
                <div class='col-xs-9'>
                    <div class='row'>
                        {keyword_search}
                    </div>""".format(advanced_search = advanced_search, keyword_search = keyword_search)

    return html


def serve_search_page(database, logged_in, message = ''):
    template_vars = {'title': 'Main Page',
                     'search_message': 'Search for keywords',
                     'message': message,
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'search')
                    }

    templater.render('search.html', template_vars)

def make_pairs(l):
    # isn't she beautiful?
    return (l[i:i+2] for i in range(0, len(l), 2))

def serve_search_results(database, logged_in, form, search_limit = SEARCH_RESULT_LIMIT):
    search_terms = {}
    for key in form:
        if key == 'result-cap':
            search_limit = int(form[key])
        else:
            search_terms[key] = form[key]

    num_matches, search_results = mekong_backend.find_all_matching_books(database, search_terms, search_limit)

    if not search_results:
        body_HTML = "<h4><span class='error'>No matches found.</span></h4>"
    else:
        results_shown = min(search_limit, num_matches)

        body_HTML = """
                    <span class='warning'>Showing 1-{results_shown} of {num_matches} results.""".format(results_shown  = results_shown, num_matches = num_matches)

        if num_matches > search_limit:
            body_HTML += " To see more results, use the Advanced Search panel."
        body_HTML += "</span>"

        for pair in make_pairs(search_results):
            body_HTML += """
                    <div class='row'>"""

            for ISBN in pair:
                book_details = mekong_backend.get_book_details(database, ISBN)
                book = []
                for info in book_details:
                    info = re.sub(r'{', '{{', str(info))
                    info = re.sub(r'}', '}}', str(info))
                    info = re.sub(r'\\', '\\\\', str(info))
                    book.append(info)
                book_details = book

                ISBN = book_details[0]

                image_url = book_details[8]

                if image_url == '-': # was not added to database, put in a placeholder image
                    image_url = 'http://www.acupuncture.org.au/portals/0/AACMAFiles/Images/Publications/Open%20Book.jpg'

                description = book_details[14]
                # the description comes pre-formatted, but I only want to show the
                # first 75 words. get rid of formatting that may be chopped in half
                # and cause problems
                description = re.sub('<[^>]*>', '', description, flags=re.I)
                # now, grab the first 75 words (if there are more than 75)
                if len(description.split()) > 75:
                    description = description.split()[:75]
                    description = ' '.join(description)
                    description += '...'

                price = book_details[20]

                name = book_details[21]

                authors = ", ".join(ast.literal_eval(book_details[18]))

                body_HTML += """
                        <div class='result'>
                            <div class='text pull-right'>
                                <a href='{base_URL}/info?ISBN={ISBN}'><b>{name}</b></a><br />
                                {authors}<br /><br />
                                <div class='image visible-xs'>
                                    <img src='{URL}'>
                                </div>

                                {description} <a href='{base_URL}/info?ISBN={ISBN}'>See more</a>
                                <br /><br />
                                {price}<br />
                                <a class='additem' ISBN='{ISBN}'>Add to basket</a>
                            </div>

                            <div class='image pull-left hidden-xs'>
                                <img src='{URL}'>
                            </div>
                        </div>""".format(base_URL = base_URL, name = name, URL = image_url,
                                         ISBN = ISBN, description = description,
                                         authors = authors, price = price)

            body_HTML += """
                    </div>"""

    template_vars = {'title': 'Search Results',
                     'results': body_HTML,
                     'search_message': 'Search for keywords',
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'search')
                    }

    templater.render('search_results.html', template_vars)

def serve_book_info(database, ISBN, logged_in):
    book_details = mekong_backend.get_book_details(database, ISBN)

    if book_details == results.FAILURE_INVALID_ISBN:
        body_HTML = """<span class='error'><h4>{message} Please try again.</h4></span>
                </div>
            </div>""".format(message = results.FAILURE_INVALID_ISBN)
    else:
        book = []
        for info in book_details:
            info = re.sub(r'{', '{{', str(info))
            info = re.sub(r'}', '}}', str(info))
            info = re.sub(r'\\', '\\\\', str(info))
            book.append(info)
        book_details = book
        ISBN = book_details[0]
        EAN = book_details[1]

        image_url = book_details[8]

        if image_url == '-': # was not added to database, put in a placeholder image
            image_url = 'http://www.acupuncture.org.au/portals/0/AACMAFiles/Images/Publications/Open%20Book.jpg'

        catalog, binding, numpages, description, publisher, publicationdate, releasedate = book_details[11:18]

        authors = ', '.join(ast.literal_eval(book_details[18]))

        salesrank, price, name, year = book_details[19:]
        body_HTML = """
                    <div class='book'>
                        <div class='image pull-left hidden-xs'>
                            <img src='{URL}'>
                        </div>

                        <div class='text'>
                            <h4><b>{name}</b></h4>
                            {authors}
                            <div class='image visible-xs'>
                                <img src='{URL}'>
                            </div>

                            {description}<br /><br />
                            {price}<br />
                            <a class='additem' ISBN='{ISBN}'>Add to basket</a>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->

            <div class='row'>
                <div class='col-xs-12'>
                    <table class='details'>
                        <tr>
                            <td>EAN</td>
                            <td>{EAN}</td>
                        </tr>
                        <tr>
                            <td>ISBN</td>
                            <td>{ISBN}</td>
                        </tr>
                        <tr>
                            <td>Sales Rank</td>
                            <td>{salesrank}</td>
                        </tr>
                        <tr>
                            <td>Catalog</td>
                            <td>{catalog}</td>
                        </tr>
                        <tr>
                            <td>Number of pages</td>
                            <td>{numpages}</td>
                        </tr>
                        <tr>
                            <td>Binding</td>
                            <td>{binding}</td>
                        </tr>
                        <tr>
                            <td>Publisher</td>
                            <td>{publisher}</td>
                        </tr>
                        <tr>
                            <td>Publication Date</td>
                            <td>{publicationdate}</td>
                        </tr>
                        <tr>
                            <td>Release Date</td>
                            <td>{releasedate}</td>
                        </tr>
                        <tr>
                            <td>Year</td>
                            <td>{year}</td>
                        </tr>
                    </table>
                </div>
            </div>""".format(binding = binding, catalog = catalog, EAN = EAN, numpages = numpages,
                             publicationdate = publicationdate, publisher = publisher, name = name,
                             URL = image_url, releasedate = releasedate, salesrank = salesrank,
                             ISBN = ISBN, year = year,
                             description = description, authors = authors, price = price)


    template_vars = {'title': 'Book Details',
                     'result': body_HTML,
                     'nav': print_nav(database, logged_in, 'info'),
                     'base_URL': base_URL
                    }

    templater.render('book_info.html', template_vars)

def display_basket_books(database, basket, history = False):
    html = ''
    for ISBN, quantity in basket.iteritems():
        book = mekong_backend.get_book_details(database, ISBN)

        html += """
                    <div class='row'>"""

        ISBN = book[0]

        if history:
            # get medium images instead of large
            image_url = book[5]
        else:
            image_url = book[8]

        if image_url == '-': # was not added to database, put in a placeholder image
            image_url = 'http://www.acupuncture.org.au/portals/0/AACMAFiles/Images/Publications/Open%20Book.jpg'

        price = book[20]

        name = book[21]

        authors = ", ".join(ast.literal_eval(book[18]))

        total_text = ''
        if quantity > 1:
            total_text = '<br />Total: ${total:.2f} <br />'.format(total = float(price[1:]) * int(quantity))

        html += """
                        <div class='col-xs-12'>
                            <div class='result'>
                                <div class='text pull-right'>
                                    <a href='{base_URL}/info?ISBN={ISBN}'><b>{name}</b></a><br />
                                    {authors}<br /><br />
                                    <div class='image visible-xs'>
                                        <img src='{URL}'>
                                    </div>
                                    <a href='{base_URL}/info?ISBN={ISBN}'>View Details</a>
                                    <br /><br />
                                    {price}{total}""".format(base_URL = base_URL, name = name,
                                                             ISBN = ISBN, authors = authors,
                                                             price = price, total = total_text,
                                                             URL = image_url)

        if history:
            html += """
                                 <form class='form-inline' onsubmit='return false;'>
                                        <div class='input-group input-group-sm'>
                                            <span class='input-group-addon input-group-addon-sm'>Quantity</span>
                                            <input readonly type='number' class='form-control input-sm' name='quantity' id='quantity' value={quantity}>
                                        </div>
                                    </form>""".format(quantity = quantity)

        else:
            html += """
                                    <div class='input-group input-group-sm'>
                                        <span class='input-group-addon input-group-addon-sm'>Quantity</span>
                                        <input type='number' class='form-control input-sm' name='quantity' id='quantity' value={quantity}>
                                        <div class='input-group-btn'>
                                            <button onclick='update("{ISBN}")' name='update' value='update' class='btn btn-primary btn-sm'>Update</button>
                                        </div>
                                        <div class='input-group-btn'>
                                            <button onclick='delete_book("{ISBN}")' name='delete' value='delete' class='btn btn-warning btn-sm'>Delete</button>
                                        </div>
                                    </div>""".format(quantity = quantity, ISBN = ISBN)
        html += """
                                </div>

                                <div class='image pull-left hidden-xs'>
                                    <img src='{URL}'>
                                </div>
                            </div>
                        </div>
                    </div>""".format(URL = image_url)
    return html

def serve_basket(database, username, cookie):
    advanced_search, keyword_search = search_page_template(basket = True)

    basket = mekong_backend.get_users_basket(database, username)

    if basket == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
        # cookie_basket may be empty, but we KNOW that basket is as there
        # was no account to grab the basket from, so overwriting it can't hurt
        basket = cookie.get_basket()

    if basket:
        body_HTML = ""
        if not username:
            body_HTML += "<span class='error'>Please note: you are not logged in. If you log in, this basket will be merged with your account's basket.</span>"

        display_books = display_basket_books(database, basket)
        total = 0

        price_regex = '(\$[^\.]*\.[^ <]*)'
        for price in re.findall(price_regex+'[^0-9<]', display_books):
            price = price.strip()
            total += float(price[1:])

        body_HTML += """
                    <div class='row'>
                        <div class='col-xs-12 col-sm-8'>
                            <h3>You have {n} item{suffix} in your basket</h3>
                        </div>
                        <div class='col-xs-12 col-sm-4 checkout'>
                            <a href='{base_URL}/checkout'><button class='btn btn-primary' >Total: ${total:.2f}. Check Out</button></a>
                        </div>
                    </div>""".format(base_URL = base_URL, total = total, n = len(basket.keys()), suffix = 's' if len(basket.keys()) > 1 else '')
        body_HTML += display_books
    else:
        body_HTML = "<h3>You currently have nothing in your basket.</h3>"
        if not username:
            body_HTML += "<span class='error'>Please note: you are not logged in.</span>"

    template_vars = {'title': 'Basket',
                     'advanced_search': advanced_search,
                     'keyword_search': keyword_search,
                     'basket': body_HTML,
                     'nav': print_nav(database, username, 'basket'),
                     'base_URL': base_URL
                    }

    templater.render('basket.html', template_vars)

def serve_checkout_page(database, user):
    basket = mekong_backend.get_users_basket(database, user)

    if not basket:
        return results.FAILURE_CHECKOUT_NO_BASKET
    if basket == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
        return basket # this should never happen. you're not allowed on this page if you aren't logged in (implies valid username)

    basket = display_basket_books(database, basket)

    user_info = mekong_backend.get_user_details(database, user)
    name = user_info[3]
    address = user_info[4:8]

    template_vars = {'title': 'Checkout',
                     'nav': print_nav(database, user, 'checkout'),
                     'basket': basket,
                     'name': name,
                     'base_URL': base_URL,
                     'address': ',<br />'.join(address)
                    }
    templater.render('checkout.html', template_vars)

def serve_order_history(database, user):
    # reverse the order history to have most recent order first
    order_history = list(reversed(mekong_backend.get_order_history(database, user)))

    user_info = mekong_backend.get_user_details(database, user)
    name = user_info[3]
    address = ',<br />'.join(user_info[4:8])

    body_HTML = ''
    if not order_history:
        body_HTML = "<h3>You have not ordered anything yet.</h3>"
    for order in order_history:
        time_submitted = time.ctime(order['timesubmitted'])
        del order['timesubmitted']
        formatted_basket = display_basket_books(database, order, history = True)
        body_HTML += """
        <div class='order'>
            <div class='row'>
                <div class='col-xs-3 pull-right hidden-xs'>
                    Order date: {date}<br />
                    Address
                    <div class='address'>
                        {name}<br />
                        {address}
                    </div>
                </div>
                <div class='col-xs-12'>
                    <div class='history-info visible-xs'>
                        Order date: {date}<br />
                        Address
                        <div class='address'>
                            {name}<br />
                            {address}
                        </div>
                    </div>
                    {order}
                </div>
            </div>
        </div> <!-- order -->
        """.format(order = formatted_basket, name = name, address = address, date = time_submitted)

    template_vars = {'title': 'Order History',
                     'nav': print_nav(database, user, 'history'),
                     'history': body_HTML,
                    }
    templater.render('history.html', template_vars)

def send_email(database, logged_in, page, reset = False):
    button_text = 'Resend Email'
    if reset:
        button_text = 'Reset Password'

    message = 'We will resend the verification email.'
    if reset:
        message = 'We will send you an email with a link to reset your password.'

    template_vars = {'title': button_text,
                     'page': page,
                     'message': message,
                     'button': button_text,
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'email')
                    }
    templater.render('send_email.html', template_vars)

def serve_email_password_reset_page(database, logged_in):
    send_email(database, logged_in, 'forgotten_password', reset = True)

def serve_resend_verification_page(database, logged_in):
    send_email(database, logged_in, 'resend_verification')

def serve_new_password_page(database, logged_in, info):
    username, email_address = info

    template_vars = {'title': 'Password Reset',
                     'username': username,
                     'email_address': email_address,
                     'base_URL': base_URL,
                     'nav': print_nav(database, logged_in, 'forgotten_password')
                    }
    templater.render('password_reset.html', template_vars)

def error(database, logged_in, message):
    template_vars = {'title': 'Error',
                     'nav': print_nav(database, logged_in, 'error'),
                     'error_message': message
                    }

    templater.render('error.html', template_vars)

def error_404(database, logged_in):
    template_vars = {'title': '404',
                     'nav': print_nav(database, logged_in, '404')
                    }

    templater.render('404.html', template_vars)

def print_nav(database, logged_in_user, current_page):
    nav_text = """
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapsing-menu" onclick="$( '.my-icon' ).toggleClass( 'icon-angle-down' ).toggleClass('icon-angle-up');">
                <span class="sr-only">Toggle navigation</span>
                <span class='my-icon icon-angle-down'></span>
            </button>
            <a class="navbar-brand" href="{base_URL}">Mekong</a>
        </div>

        <div class="collapse navbar-collapse collapsing-menu">
            <ul class="nav navbar-nav">""".format(base_URL = base_URL)

    for name, link, icon in (('Search', 'search', 'icon-magnifier'), ('Basket', 'basket', 'icon-shopping-cart'), ('Order History', 'history', 'icon-history'), ('Sign in/Register', 'login', 'icon-enter')):
        if (name == 'Order History') and not logged_in_user:
            continue
        if name == 'Sign in/Register' and logged_in_user:
            continue
        if current_page.lower() in name.lower():
            nav_text += """
                    <li class="active"><a href="{base_URL}/{link}"><span class='{icon}'></span>{page}</a></li>""".format(base_URL = base_URL, link = link, icon = icon, page = name)
        else:
            nav_text += """
                    <li><a href="{base_URL}/{link}"><span class='{icon}'></span>{page}</a></li>""".format(base_URL = base_URL, link = link, icon = icon, page = name)

    nav_text += """
            </ul>"""
    if logged_in_user:
        fullname = mekong_backend.fullname_from_username(database, logged_in_user)
        if fullname == results.FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD:
            # note: something has gone really wrong. user is signed in but
            # doesn't exist in database. I could raise an alert, but this
            # should actually NEVER happen.
            fullname = 'user'

        nav_text += """
            <ul class="nav navbar-nav navbar-right">
                <div class='nav-text'>
                    <span class='inline-span hidden-sm'>Welcome, {name}. Not you? </span><a href='{base_URL}/signout'>Sign Out<span class='icon-exit'></span></a>
                </div>
            </ul>""".format(base_URL = base_URL, name = fullname)

    nav_text += """
        </div>
    </nav>"""

    return nav_text
