from mekong_general import base_URL

## ACCOUNT FAILURES
FAILURE_ACCOUNT_NOT_VERIFIED = 'Account has not been verified. Please check your emails. If you did not receive a validation email, visit <a href="resend_verification">this link</a>.'
FAILURE_ACCOUNT_NO_VERIFICATION_STRING = 'A serious error has occurred - your account information has been corrupted. Reset your password (link on the Sign In/Register page) to fix this problem.'

FAILURE_ACCOUNT_INCORRECT_USERNAME_OR_PASSWORD = 'Incorrect username or password'
FAILURE_ACCOUNT_BASKET_DOESNT_EXIST = 'There is nothing in your basket. If you believe that you are receiving this message in error, please contact us at customerservice@mekong.com'

## ACCOUNT SUCCESSES
SUCCESS_ACCOUNT_OKAY = 60
SUCCESS_ACCOUNT_VERIFIED = 61
SUCCESS_ACCOUNT_CORRECT_PASSWORD = 62

## VERIFICATION FAILURES
FAILURE_VERIFICATION_IN_PROGRESS = 10
FAILURE_VERIFICATION_TIMEOUT = 'This link is no longer valid. Click <a href="{base_URL}/resend_verification">here</a> to send a new verification email.'.format(base_URL = base_URL)
FAILURE_VERIFICATION_STRING_DOESNT_EXIST = 'This link is no longer valid. Click <a href="{base_URL}/forgotten_password">here</a> to resend verification email.'.format(base_URL = base_URL)
FAILURE_PASSWORD_RESET_TIMEOUT = 'This link is no longer valid. Click <a href="{base_URL}/resend_verification">here</a> to send a new verification email.'.format(base_URL = base_URL)

## VERIFICATION SUCCESSES
SUCCESS_VERIFICATION_EMAIL_SENT = 70

## EMAIL FAILURES
FAILURE_EMAIL_DOESNT_EXIST = 'Email address not in database.'

## EMAIL SUCCESSES

## COOKIE FAILURES
FAILURE_COOKIE_LOGGED_IN = 30

## COOKIE SUCCESSES

## ISBN FAILURES
FAILURE_INVALID_ISBN = 'That is not a valid ISBN.'

## ISBN SUCCESSES

## CHECKOUT FAILURES
FAILURE_CHECKOUT_NO_BASKET = 'There is no basket. If you are receiving this message in error, please contact us at customerservice@mekong.com'
