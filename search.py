#!/usr/bin/python2.7

import json
import sqlite3
import cgi
import ast

NUM_RESULTS = 10

print "Content-type: text/html"
print

database = sqlite3.connect('allInformation.db')
cursor = database.cursor()

query = cgi.FieldStorage()['query'].value
query_type = cgi.FieldStorage()['type'].value

if query_type == 'title':
    query_string = "SELECT title FROM Books WHERE title LIKE ?"
if query_type == 'author':
    query_string = "SELECT authors FROM Books WHERE authors LIKE ?"
if query_type == 'publisher':
    query_string = "SELECT publisher FROM Books WHERE publisher LIKE ?"
if query_type == 'ISBN':
    query_string = "SELECT ISBN FROM Books WHERE ISBN LIKE ?"

cursor.execute(query_string, ('%%'+query+'%%',))

ret = []
if query_type == 'author':
    for data in cursor.fetchmany(NUM_RESULTS):
        ret.append(", ".join(ast.literal_eval(data[0])))
elif query_type == 'ISBN':
    for data in cursor.fetchmany(NUM_RESULTS):
        ret.append(str(data[0]))
else:
    for data in cursor.fetchmany(NUM_RESULTS):
        ret.append(data[0])

print json.dumps(ret)
