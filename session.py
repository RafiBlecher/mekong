#!/usr/bin/python2.7

## Adapted from http://webpython.codepoint.net/cgi_session_class

import sha, shelve, time, Cookie, os, hmac, hashlib
from mekong_general import base_URL

TIMEOUT_MINUTES = 45

class MyCookie(Cookie.SimpleCookie):
    def value_encode(self, val):
        b = hmac.new('suchasecretkey', str(val), hashlib.sha256).hexdigest()
        return val, str(val) + b

    def value_decode(self, val):
        a, b = val[:-64], val[-64:]
        if hmac.new('suchasecretkey', a, hashlib.sha256).hexdigest() == b:
            return a, val
        else:
            return '', val

class Session(object):
    def __init__(self):
        string_cookie = os.environ.get('HTTP_COOKIE', '')
        self.cookie = MyCookie()
        self.cookie.load(string_cookie)

        if self.cookie.get('session'):
            sid = self.cookie['session'].value
            # Clear session cookie from other cookies
            self.cookie.clear()
        else:
            self.cookie.clear()
            sid = sha.new(repr(time.time())).hexdigest()

        self.data = shelve.open('session/sess_' + sid, writeback=True)

        self.cookie['session'] = sid
        self.cookie['session']['path'] = base_URL

        rememberme = self.data.get('rememberme')
        # set cookie to expire at end of session if necessary
        if rememberme == '0':
            self.cookie['session']['expires'] = None
        else:
            # cookie expires in a year from the last time you accessed a page
            # unless you didn't select 'Remember Me' when you log in. This is
            # so temporary baskets can exist
            self.cookie['session']['expires'] = 60*60*24*365

        if not self.data.get('logged_in'):
            self.data['logged_in'] = False
        if not self.data.get('basket'):
            self.data['basket'] = {}
      
    def update(self, username, persistent_cookie = False):
        if persistent_cookie:
            self.data['rememberme'] = '1'
        else:
            self.data['rememberme'] = '0'
        self.data['logged_in'] = True
        self.data['username'] = username

    def is_logged_in(self):
        if self.data['logged_in']:
            return self.data['username']
        return False
    
    def get_basket(self):
        return self.data['basket']

    def expire_cookie(self):
        # This is so temporary baskets can exist
        self.cookie['session']['expires'] = 60*60*24*365
        self.data['logged_in'] = False
        self.data['username'] = ''
        self.cookie['remember'] = ''

    def details(self):
        return self.cookie
