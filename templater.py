import re

def render(page, template_vars):
    page = open('static/'+page).read()
    for template_var in re.findall('{{ [^}]* }}', page):
        try:
            replacement = template_vars[template_var[3:-3]]
        except KeyError:
            continue
        page = re.sub(template_var, replacement, page)

    print page
